package org.odvs.domain;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class RestMessage implements Serializable{

	private boolean success;
	private String message;
	private Map<String, Object> data;

	public RestMessage() {
		this.data = new HashMap<String, Object>();
	}
	public RestMessage( boolean success ) {
		this();
		this.success = success;
	}

	public RestMessage(boolean success, String message) {
		super();
		this.success = success;
		this.message = message;
		this.data = new HashMap<String, Object>();
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Map<String, Object> getData() {
		return data;
	}

	public void setData(Map<String, Object> data) {
		this.data = data;
	}

	public boolean containsKey(Object arg0) {
		return data.containsKey(arg0);
	}

	public boolean containsValue(Object arg0) {
		return data.containsValue(arg0);
	}

	public Object get(Object arg0) {
		return data.get(arg0);
	}

	public Object put(String arg0, Object arg1) {
		return data.put(arg0, arg1);
	}

	public Object remove(Object arg0) {
		return data.remove(arg0);
	}
}
