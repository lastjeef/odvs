<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>

<!DOCTYPE html>
<html lang="en">

<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>CO2 Ocean Data Viewer Tableau de bord</title>



</script>
<script type="text/javascript"
	src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>

<!-- Bootstrap Core CSS -->
<link href='<c:url value="/resources/css/bootstrap.min.css" />'
	rel="stylesheet">

<!-- Custom CSS -->
<link href='<c:url value="/resources/css/sb-admin.css" />'
	rel="stylesheet">

<!-- Morris Charts CSS -->
<link href='<c:url value="/resources/css/plugins/morris.css" />'
	rel="stylesheet">

<!-- Custom Fonts -->
<link
	href='<c:url value="/resources/font-awesome/css/font-awesome.min.css" />'
	rel="stylesheet" type="text/css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src='<c:url value="/resources/https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"/>'></script>
        <script src='<c:url value="/resources/https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"/>'></script>
    <![endif]-->

</head>

<body>

	<div id="wrapper">

		<!-- Navigation -->
		<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target=".navbar-ex1-collapse">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="index.html">Ocean Data View Server</a>
			</div>
			<!-- Top Menu Items -->

			<!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
			<div class="collapse navbar-collapse navbar-ex1-collapse">
				<ul class="nav navbar-nav side-nav">
					<li class="active"><a href="index"><i
							class="fa fa-fw fa-dashboard"></i> Tableau de bord</a></li>
				</ul>
			</div>
			<!-- /.navbar-collapse -->
		</nav>

		<div id="page-wrapper">

			<div class="container-fluid">

				<!-- Page Heading -->
				<div class="row">
					<div class="col-lg-12">
						<h1 class="page-header">
							Tableau de bord <small>Ocean Map</small>
						</h1>
						<!-- 						<ol class="breadcrumb"> -->
						<!-- 							<li class="active"><i class="fa fa-dashboard"></i> Carte</li> -->
						<!-- 						</ol> -->
					</div>
				</div>
				<!-- /.row -->


				<!-- /.row -->

				<div class="row" id="data">
					<div class="col-lg-6">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3 class="panel-title">
									<i class="fa fa-bar-chart-o fa-fw"></i> points de prelevements
								</h3>
							</div>
							<div class="panel-body">
								<div id="map_container"></div>
							</div>
						</div>
					</div>
					<div class="col-lg-6">



						<div class="row">

							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title">
										<i class="fa fa-long-arrow-right fa-fw"></i> Graphe
									</h3>
								</div>
								<div class="panel-body">

									<div id="chart1" style="height: 200px; width: 500px"></div>

									<div class="text-right"></div>
								</div>
							</div>

							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title">
										<i class="fa fa-long-arrow-right fa-fw"></i> Station /
										Profondeurs
									</h3>
								</div>
								<div class="panel-body" id="stationform"></div>


								<div class="text-right"></div>
							</div>
						</div>
					</div>



					<div class="row" id="data">

						<div class="col-lg-5">
							<div class="row">

								<div class="panel panel-default">
									<div class="panel-heading">
										<h3 class="panel-title">
											<i class="fa fa-long-arrow-right fa-fw"></i> Données
										</h3>
									</div>
									<div class="panel-body">
										<form id="dataPointForm">

											<div class="col-lg-3">


												<div class="form-group">
													<label for="pco2_sst" data-toggle="tooltip"
														data-html="true" title="AnthropogenicCO2n">ACO2 </label> <input
														type="text" class="form-control" name="aco2">
												</div>

												<div class="form-group">
													<label for="oxygen" data-toggle="tooltip" data-html="true"
														title="Oxygen">Oxy</label> <input type="text"
														class="form-control" name="oxygen">
												</div>
											</div>
											<div class="col-lg-3">

												<div class="form-group">
													<label for="pco2_teq" data-toggle="tooltip"
														data-html="true" title="Total CO2">TCO2 </label> <input
														type="text" class="form-control" name="tco2">
												</div>

												<div class="form-group">
													<label for="salinity" data-toggle="tooltip"
														data-html="true" title="Salinity">SAL</label> <input
														type="text" class="form-control" name="salinity">
												</div>
											</div>
											<div class="col-lg-3">

												<div class="form-group">
													<label for="temperature" data-toggle="tooltip"
														data-html="true" title="Temperature in ºC">TEMP</label> <input
														type="text" class="form-control" name="temperature">
												</div>

												<div class="form-group">
													<label for="alkalinity" data-toggle="tooltip"
														data-html="true" title="Alcaklinity">Alk </label> <input
														type="text" class="form-control" name="alkalinity">
												</div>
											</div>
											<div class="col-lg-3">
												<div class="form-group">
													<label for="pressure" data-toggle="tooltip"
														data-html="true" title="Pressure">Pres</label> <input
														type="text" class="form-control" name="pressure">
												</div>

												<div class="form-group">
													<label for="ph" data-toggle="tooltip" data-html="true"
														title="Computed PH">PH</label> <input type="text"
														class="form-control" name="ph" id="phd">
												</div>
											</div>

											<div class="col-lg-3">
												<div class="form-group">
													<label for="exampleInputName2">Mettre à jour </label>
													<button id="validerPointButton" type="button"
														class="btn btn-primary">Valider</button>
												</div>

											</div>
										</form>
									</div>


									<div class="text-right"></div>
								</div>
							</div>
						</div>


					</div>
					<!-- /.row -->

					<div class="row"></div>
					<!-- /.row -->

				</div>
				<!-- /.container-fluid -->

			</div>
			<!-- /#page-wrapper -->

		</div>
		<!-- /#wrapper -->

		<style type="text/css">
div#map_container {
	width: 100%;
	height: 60%;
}
</style>


		<!-- jQuery -->
		<script src='<c:url value="/resources/js/jquery.js"/>'></script>

		<!-- Bootstrap Core JavaScript -->
		<script src='<c:url value="/resources/js/bootstrap.min.js"/>'></script>

		<!-- Morris Charts JavaScript -->
		<script
			src='<c:url value="/resources/js/plugins/morris/raphael.min.js"/>'></script>
		<script
			src='<c:url value="/resources/js/plugins/morris/morris.min.js"/>'></script>


		<script
			src='<c:url value="/resources/js/plugins/flot/jquery.flot.js"/>'></script>
		<script
			src='<c:url value="/resources/js/plugins/flot/jquery.flot.tooltip.min.js"/>'></script>
		<script
			src='<c:url value="/resources/js/plugins/flot/jquery.flot.resize.js"/>'></script>
		<script
			src='<c:url value="/resources/js/plugins/flot/jquery.flot.pie.js"/>'></script>

		<script src='<c:url value="/resources/js/handlebars.js"/>'></script>
		<script src='<c:url value="/resources/js/form2js.js"/>'></script>
		<script src='<c:url value="/resources/js/js2form.js"/>'></script>
		<script src='<c:url value="/resources/js/odvs/MapMarkers.js"/>'></script>
		<script src='<c:url value="/resources/js/odvs/DataPoint.js"/>'></script>
		<script src='<c:url value="/resources/js/odvs/Chart2d.js"/>'></script>


		<script>
			jQuery(document).ready(function() {
				$('[data-toggle="tooltip"]').tooltip()

				loadMap();
				loadPoint();
				chart2d();

			});
		</script>

		<button id="detailsPointButton" type="button" class="btn btn-primary">Details</button>
</body>


<script type="text/x-handlebars" id="station-template">


{{#points}}
<form >

	<div class="col-lg-1">

		<div class="form-group">
			<button type="button" class="btn btn-primary" onclick="loadPoint({{idPoint}})">{{depth}}</button>
		</div>

	</div>
</form>

{{/points}}

</script>




</html>
