package org.odvs.utils;

import java.io.Serializable;

import org.springframework.web.multipart.MultipartFile;

public class FileUpload implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7194210509079071123L;

	private MultipartFile file;

	public MultipartFile getFile() {
		return file;
	}

	public void setFile(MultipartFile file) {
		this.file = file;
	}

}
