package org.odvs.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;


public class DateUtils {

	public static String FORMAT_1 = "dd-MM-yyyy HH:mm:ss";
	public static String FORMAT_2 = "dd-MM-yyyy";
	public static String FORMAT_3 = "dd/MM/yyyy";
	public static String FORMAT_4 = "dd/MM";
	public static String FORMAT_HEURE_1 = "HH:mm";
	public static int EQAUL = 0;
	public static int AFTER = 1;
	public static int BEFORE = -1;
	
	
	public static Date getDate() {
		return Calendar.getInstance().getTime();
	}
	
	public static Date getDateFirstHour() {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		return calendar.getTime();
	}
	
	public static Date getDateFirstHour( Date currentDate ) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime( currentDate );
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		return calendar.getTime();
	} 
	public static Date getDateLastHour( Date currentDate ) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime( currentDate );
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		return calendar.getTime();
	}
	public static Date getDateLastHour() {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		return calendar.getTime();
	}
	
	public String getTimeString(Integer hour, Integer minute) {
		Date date = new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.HOUR_OF_DAY, hour);
		cal.set(Calendar.MINUTE, minute);
		cal.set(Calendar.SECOND, 0);
		date = cal.getTime();
		SimpleDateFormat sdfDate = new SimpleDateFormat("HH:mm");
		String strDate = sdfDate.format(date);
		System.out.println(strDate);
		return strDate;
	}

	public static Date addTimeToDate(Date date, Date time) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(time);
		int hours = calendar.get(Calendar.HOUR_OF_DAY);
		int minutes = calendar.get(Calendar.MINUTE);
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.HOUR_OF_DAY, hours);
		cal.set(Calendar.MINUTE, minutes);
		cal.set(Calendar.SECOND, 0);
		date = cal.getTime();
		return date;
	}
	public static Date addTimeToDate(Date date, String retard) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int minutes = cal.get(Calendar.MINUTE);
		cal.set(Calendar.MINUTE,minutes + Integer.parseInt(retard));
		date = cal.getTime();
		return date;
	}

	public static Date addTimeRouteToDate(Date date, long duree) {
		//System.out.println("date au d?part : "+date);
		//System.out.println("dur?e en secondes : "+duree);
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int secondes = cal.get(Calendar.SECOND);
		cal.set(Calendar.SECOND,secondes + new Long(duree).intValue());
		date = cal.getTime();
		//System.out.println("date apr?s calcul : "+date);
		return date;
		
	}
	public static Date addTimeToDate(Date date, Integer minute) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.SECOND,0);
		cal.set(Calendar.MILLISECOND,0);
		int minutes = cal.get(Calendar.MINUTE);
		cal.set(Calendar.MINUTE,minutes + minute);
		date = cal.getTime();
		return date;
	}
	public static Date soustractTimeToDate(Date date, String retard) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int minutes = cal.get(Calendar.MINUTE);
		cal.set(Calendar.MINUTE,minutes - Integer.parseInt(retard));
		date = cal.getTime();
		return date;
	}
	public static Date formatStringToDate(String strDate){
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		Date date=null;
		try {
			date = formatter.parse(strDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		System.out.println(date);
		return date;
	}
	
	public static String dateToString( Date date, String format ) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat( format );
		if( date != null ) {
			return simpleDateFormat.format( date );
		}
		return null;
	}
	
	public static String dateToString( Date date ) {
		return dateToString( date, FORMAT_1);
	}
	
	public static String DateAndRecurrenceToString( Date date, boolean isRecurrence ) {
		SimpleDateFormat simpleDateFormat = null;
		if( !isRecurrence ) {
			simpleDateFormat = new SimpleDateFormat( FORMAT_3 );
		}else{
			simpleDateFormat = new SimpleDateFormat( FORMAT_4 );
		}
		return simpleDateFormat.format( date );
			
	}
	
	
	public static boolean comparewithLastDayOfMonth(){
		Calendar c = Calendar.getInstance();
		Date date = new Date();
		c.setTime(date);
		c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
		return isEqual(new Date(), c.getTime());
	}
	
	/**
	 * test si la date {@code from} est �gale � la date {@code to}.
	 * @param from
	 * @param to
	 * @return
	 */
	
	
	public static boolean isEqual( Date from, Date to ){
		String formatFrom = new SimpleDateFormat( "dd/MM/yyyy" ).format( from );
		String formatTo = new SimpleDateFormat( "dd/MM/yyyy" ).format( to );
		
		int isEqual = formatFrom.equals( formatTo ) ? 0 : -1;
		if( isEqual == 0  ) {
			return true;
		}
		
		return false;
	}
	
	public static int compare( Date d1, Date d2 ) {
		int result = EQAUL;
		Calendar cal1 = Calendar.getInstance();
    	Calendar cal2 = Calendar.getInstance();
    	cal1.setTime( d1 );
    	cal2.setTime( d2 );
    	if(cal1.after(cal2)){
    		result = AFTER;
    	}
    	if(cal1.before(cal2)){
    		result = BEFORE;
    	}
		return result;
	}
	
	public static long getDiffByDay( Date d1, Date d2 ) {
		if( d1 == null || d2 == null ) {
			return -1;
		}
		long result = 0;
		long diff = d1.getTime() - d2.getTime();
		result = TimeUnit.DAYS.convert( diff, TimeUnit.MILLISECONDS );
		return result;
	}
	
	
	
	/**
	 * test si la date �gale � la date du jour si oui elle affiche seulement l'heure sinon la fonction affiche la date en format "MM/dd/yyyy à HH:mm:ss"
	 * @param dateToTrans
	 * @return
	 */
	public static String transformeDate( Date dateToTrans ) {
		StringBuilder result = new StringBuilder();
		boolean isEq = isEqual( dateToTrans, Calendar.getInstance().getTime() );
		if( isEq ) { 
			result.append( new SimpleDateFormat( "HH" ).format( dateToTrans ) ).append( "h" ).append(  new SimpleDateFormat( "mm" ).format( dateToTrans ) );
		} else {
			result.append( new SimpleDateFormat( "dd/MM/yyyy" ).format( dateToTrans ) ).append( " à " ).append( new SimpleDateFormat( "HH" ).format( dateToTrans ) ).append( "h" ).append(  new SimpleDateFormat( "mm" ).format( dateToTrans ) );
		}
		return result.toString();
	}
	
	public static String toHeurAndMinute( Date date ) {
		if( date == null ) return null;
		return new StringBuilder( new SimpleDateFormat( "HH" ).format( date ) ).append( "h" ).append( new SimpleDateFormat( "mm" ).format( date ) ).toString();
	}
	
	public static int getDayOfWeek(Date date){
		if(date!=null){
		Calendar c = Calendar.getInstance(Locale.FRANCE);
		c.setTime(date);
		return c.get(Calendar.DAY_OF_WEEK);
		}
		return 0;
	}
	
	public static long getDiffByDayInMinutes( Date d1, Date d2 ) {
		if( d1 == null || d2 == null ) {
			return -1;
		}
		long diff = d1.getTime() - d2.getTime();
		      
		long diffMinutes = diff / (60 * 1000);         
		
		return diffMinutes;
	}
	 

	public static Date addTimeInMinutesToDate(Date date, int retard) {
		//System.out.println("date au d?part : "+date);
		//System.out.println("dur?e en minutes : "+retard);
		if (retard==0)  {
			//System.out.println("date apr?s calcul : "+date);
			return date;
		}
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int minutes = cal.get(Calendar.MINUTE);
		cal.set(Calendar.MINUTE,minutes + retard);
		date = cal.getTime();
		//System.out.println("date apr?s calcul : "+date);
		return date;
	}
	
	
	public static boolean isDateFormatValid( String date ){
		boolean isValid = true;
		try{
			SimpleDateFormat sdf = new SimpleDateFormat( FORMAT_3 );
			sdf.parse( date );
		}catch( ParseException e ){
			isValid = false;
		}
		return isValid;
	}
	
	/**
	 * convert un objet string vers un objet date et le format utilisé est dd/MM/yyyy
	 * @param dateString
	 * @return
	 */
	public static Date  stringToDate( String dateString ) {
		try{
			SimpleDateFormat sdf = new SimpleDateFormat( FORMAT_3 );
			Date date = sdf.parse( dateString );
			return date;
		}catch( ParseException e ){
			return null;
		}
	}

}
