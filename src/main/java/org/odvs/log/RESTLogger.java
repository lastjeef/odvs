package org.odvs.log;

import java.util.Calendar;

import org.apache.log4j.Logger;
import org.apache.log4j.Priority;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class RESTLogger {

	private final static Logger LOGGER = Logger
			.getLogger("Controller Exceptions");

	@AfterThrowing(pointcut = "execution(* org.odvs.rest.*.*(..))", throwing = "error")
	public void afterThrow(JoinPoint joinPoint, Throwable error) {
		Object[] args = joinPoint.getArgs();
		String claz = joinPoint.getTarget().getClass().getSimpleName();
		String name = joinPoint.getSignature().getName();

		StringBuffer sb = new StringBuffer(claz + ".");
		sb.append(name + "(");

		sb.append(")");
		sb.append("Exception : ");
		sb.append(error);
		LOGGER.error(sb);
	}

}
