package org.odvs.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
//import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "point")
public class LightPoint implements Serializable {

	private static final long serialVersionUID = -602399350200770429L;

	@Id
	@Column(name = "idpoint")
	private Integer idPoint;
	private String section;
	private Integer station;
	private String latitude;
	private String longitude;

	public LightPoint() {
		// TODO Auto-generated constructor stub
	}

	public LightPoint(Integer idPoint, String section, Integer station,
			String latitude, String longitude) {
		super();
		this.idPoint = idPoint;
		this.section = section;
		this.station = station;
		this.latitude = latitude;
		this.longitude = longitude;

	}

	public Integer getIdPoint() {
		return idPoint;
	}

	public void setIdPoint(Integer idPoint) {
		this.idPoint = idPoint;
	}

	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public Integer getStation() {
		return station;
	}

	public void setStation(Integer station) {
		this.station = station;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	

}
