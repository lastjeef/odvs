package org.odvs.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import javax.transaction.Transactional;

import org.odvs.dao.PointDAO;
import org.odvs.domain.LightPoint;
import org.odvs.domain.Point;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PointService {

	@Autowired
	private PointDAO pointDao;

	public Point addPoint(Point point) {
		return pointDao.persist(point);
	}

	@Transactional
	public List<Point> getAllPoint() {
		return pointDao.listPoint();
	}

	@Transactional
	public List<LightPoint> getAllLightPoints() {
		HashMap<String, LightPoint> Dpoints = new HashMap<String, LightPoint>();
		for (LightPoint p : pointDao.listLightPoint()) {
			Dpoints.put(p.getStation() + p.getSection(), p);
		}
		List<LightPoint> points = new ArrayList<LightPoint>();
		for (LightPoint lightPoint : Dpoints.values()) {
			points.add(lightPoint);
		}


		return points;
	}

	@Transactional
	public List<Point> getAllPoint(String lat, String lon) {
		return pointDao.listPoint(lat, lon);
	}

	@Transactional
	public List<Point> getAllPoint(Integer station) {
		return pointDao.listPoint(station);
	}

	public void deletePoint(Integer idPoint) {
		pointDao.removePoint(idPoint);
	}

	public Point getById(Integer idPoint) {

		return pointDao.findById(idPoint);
	}

	@Transactional
	public Point updatePoint(Point Point) {
		return pointDao.updatePoint(Point);
	}

}
