package org.odvs.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
//import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "point")
public class Point implements Serializable {

	private static final long serialVersionUID = -602399350200770429L;

	@Id
	@Column(name = "idpoint")
	private Integer idPoint;
	private String section;
	private Integer station;
	private String latitude;
	private String longitude;
	private String day;
	private String month;
	private String year;
	private Double pressure;
	private Double depth;
	private Double temperature;
	private Double salinity;
	private Double oxygen;
	private Double tco2;
	private Double alkalinity;
	@Column(name = "AnthropogenicCO2")
	private Double aco2;

	public Point() {
		// TODO Auto-generated constructor stub
	}

	public Point(Integer idPoint, String section, Integer station,
			String latitude, String longitude, String day, String month,
			String year, Double pressure, Double depth, Double temperature,
			Double salinity, Double oxygen, Double tco2, Double alkalinity,
			Double aco2) {
		super();
		this.idPoint = idPoint;
		this.section = section;
		this.station = station;
		this.latitude = latitude;
		this.longitude = longitude;
		this.day = day;
		this.month = month;
		this.year = year;
		this.pressure = pressure;
		this.depth = depth;
		this.temperature = temperature;
		this.salinity = salinity;
		this.oxygen = oxygen;
		this.tco2 = tco2;
		this.alkalinity = alkalinity;
		this.aco2 = aco2;
	}

	public Integer getIdPoint() {
		return idPoint;
	}

	public void setIdPoint(Integer idPoint) {
		this.idPoint = idPoint;
	}

	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public Integer getStation() {
		return station;
	}

	public void setStation(Integer station) {
		this.station = station;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public Double getPressure() {
		return pressure;
	}

	public void setPressure(Double pressure) {
		this.pressure = pressure;
	}

	public Double getDepth() {
		return depth;
	}

	public void setDepth(Double depth) {
		this.depth = depth;
	}

	public Double getTemperature() {
		return temperature;
	}

	public void setTemperature(Double temperature) {
		this.temperature = temperature;
	}

	public Double getSalinity() {
		return salinity;
	}

	public void setSalinity(Double salinity) {
		this.salinity = salinity;
	}

	public Double getOxygen() {
		return oxygen;
	}

	public void setOxygen(Double oxygen) {
		this.oxygen = oxygen;
	}

	public Double getTco2() {
		return tco2;
	}

	public void setTco2(Double tco2) {
		this.tco2 = tco2;
	}

	public Double getAlkalinity() {
		return alkalinity;
	}

	public void setAlkalinity(Double alkalinity) {
		this.alkalinity = alkalinity;
	}

	public Double getAco2() {
		return aco2;
	}

	public void setAco2(Double aco2) {
		this.aco2 = aco2;
	}

	@Override
	public String toString() {
		return "Point [idPoint=" + idPoint + ", section=" + section
				+ ", station=" + station + ", latitude=" + latitude
				+ ", longitude=" + longitude + ", day=" + day + ", month="
				+ month + ", year=" + year + ", pressure=" + pressure
				+ ", depth=" + depth + ", temperature=" + temperature
				+ ", salinity=" + salinity + ", oxygen=" + oxygen + ", tco2="
				+ tco2 + ", alkalinity=" + alkalinity + ", aco2=" + aco2 + "]";
	}

}
