/**
 * 
 */

function chart2d(data) {

	// p = form2js('dataPointForm', '.', true, null);
	// console.log("chart : ")
	$('#chart1').empty();
	new Morris.Line({
		// ID of the element in which to draw the chart.
		element : 'chart1',
		// Chart data records -- each entry in this array corresponds to a point
		// on
		// the chart.
		data : data.points,
		// The name of the data record attribute that contains x-values.
		xkey : 'depth',
		// A list of names of data record attributes that contain y-values.
		ykeys : [ 'temperature', 'oxygen', 'salinity' ], // Labels for the
		// ykeys -- will be
		// displayed when
		// you hover over
		// the
		// chart.
		labels : [ 'Temperature', 'Oxygen', 'Salinity' ],
		parseTime : false,
	});

}
