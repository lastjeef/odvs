/**
 * 
 */

function loadMap() {

	console.log("Load Map");
	var latlng = new google.maps.LatLng(-56.333, -78.232);
	var myOptions = {
		zoom : 1,
		center : latlng,
		mapTypeId : google.maps.MapTypeId.ROADMAP
	};
	var map = new google.maps.Map(document.getElementById("map_container"),
			myOptions);
	var points;

	$.ajax({
		type : 'GET',
		url : "map",
		success : function(result) {
			points = result.points;

			var p;
			var pinfo = false;

			for (var i = 0; i < points.length; i++) {
				p = points[i];

				var position = {
					lat : parseFloat(p.latitude),
					lng : parseFloat(p.longitude)
				};

				var info = new google.maps.InfoWindow({
					content : constructMarker(p)
				});

				var marker = new google.maps.Marker({
					position : position,
					map : map,
					icon : '/ODVServerProject/resources/images/co2.png',
					infowindow : info
				});

				marker.addListener('click', function() {
					if (pinfo) {
						pinfo.close();
					}
					this.infowindow.open(map, this);
					pinfo = this.infowindow;
				});

			}
		}
	});

}

function constructMarker(p) {
	var result = '<ul>' + '<li>Position  : '
			+ convertDMS(parseFloat(p.latitude), parseFloat(p.longitude)) + '</li>'
			+ '<li><button id="detailPointButton" onclick="loadStation(' + p.station
			+ ')" type="button"class="btn btn-primary">Details</button>'
			+ '</ul>'
	return result;
}

function convertDMS(lat, lng) {

	var convertLat = Math.abs(lat);
	var LatDeg = Math.floor(convertLat);
	var LatSec = (Math.floor((convertLat - LatDeg) * 60));
	var LatCardinal = ((lat > 0) ? "N" : "S");

	var convertLng = Math.abs(lng);
	var LngDeg = Math.floor(convertLng);
	var LngSec = (Math.floor((convertLng - LngDeg) * 60));
	var LngCardinal = ((lng > 0) ? "E" : "W");

	return LatDeg + LatCardinal + LatSec + " " + LngDeg + LngCardinal
			+ LngSec;
}