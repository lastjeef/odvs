package org.odvs.log;

import java.util.Calendar;

import org.apache.log4j.Logger;
import org.apache.log4j.Priority;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class DAOLogger {

	private final static Logger LOGGER = Logger.getLogger("DAO Exceptions");

	@AfterThrowing(pointcut = "execution(* org.odvs.dao.*.*(..))", throwing = "error")
	public void afterThrow(JoinPoint joinPoint, Throwable error) {
		Object[] args = joinPoint.getArgs();
		String claz = joinPoint.getTarget().getClass().getSimpleName();
		String name = joinPoint.getSignature().getName();

		StringBuffer sb = new StringBuffer(claz + ".");
		sb.append(name + "(");


		sb.append(")");
		sb.append("Exception : ");

		sb.append(error);
		System.err.println(error);
		LOGGER.error(sb);
	}

}
