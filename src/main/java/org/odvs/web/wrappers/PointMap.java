package org.odvs.web.wrappers;

import java.util.Calendar;

import org.odvs.domain.Point;

public class PointMap {

	Integer id;
	Float lat;
	Float lon;
	String date;

	public PointMap() {
		// TODO Auto-generated constructor stub
	}

	public PointMap(Point p) {
		this.id = p.getIdPoint();
		this.lat = Float.parseFloat(p.getLatitude());
		this.lon = Float.parseFloat(p.getLongitude());
		this.date = p.getDay() + "/" + p.getMonth() + "/" + p.getYear();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Float getLat() {
		return lat;
	}

	public void setLat(Float lat) {
		this.lat = lat;
	}

	public Float getLon() {
		return lon;
	}

	public void setLon(Float lon) {
		this.lon = lon;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

}
