/**
 * 
 */

function loadPoint(id) {
	$.ajax({
		type : 'POST',
		url : "point",
		data : {
			id : id,
		},
		success : function(result) {
			console.log(result);
			var point = result.point;
			$('#phd').val(7);
			js2form(document.getElementById('dataPointForm'), point);
		}
	});

}

function loadStation(station) {
	$.ajax({
		type : 'POST',
		url : "station",
		data : {
			station : station,
		},
		success : function(result) {
			console.log(result);
			var template = Handlebars.compile($("#station-template").text());
			var html = template(result);
			$('#stationform').html(html);
			chart2d(result);
		}
	});

}

//
// var categorie = form2js(
// 'formCategorie', '.', true,
// null);
