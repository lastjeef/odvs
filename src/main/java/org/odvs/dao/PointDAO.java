package org.odvs.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.odvs.domain.LightPoint;
import org.odvs.domain.Point;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class PointDAO {

	@PersistenceContext
	private EntityManager em;

	public Point persist(Point point) {
		em.persist(point);
		em.flush();
		return point;
	}

	public List<Point> listPoint() {

		return em.createQuery("select p  from Point  p", Point.class)
				.getResultList();
	}

	public List<LightPoint> listLightPoint() {

		return em
				.createQuery(
						"select p  from LightPoint  p where p.station in ( select DISTINCT(lp.station) from LightPoint lp)",
						LightPoint.class).getResultList();
	}

	public List<Point> listPoint(String lat, String lon) {

		return em
				.createQuery(
						"select p  from Point  p  where p.lat=:lat  and p.lon=:lon",
						Point.class).setParameter("lat", lat)
				.setParameter("lon", lon).getResultList();
	}

	public List<Point> listPoint(Integer station) {

		return em
				.createQuery(
						"select p  from Point  p  where p.station=:station order by p.depth",
						Point.class).setParameter("station", station)
				.getResultList();
	}

	public Point findById(Integer idPoint) {
		Point point = em.find(Point.class, idPoint);
		return point;
	}

	public Point addPoint(Point point) {
		final Point merged = em.merge(point);
		em.flush();
		return merged;
	}

	public Point updatePoint(Point point) {
		final Point merged = em.merge(point);
		em.flush();
		return merged;
	}

	public void removePoint(Integer id) {
		final Point point = em.find(Point.class, id);
		System.out.println(point);
		try {

			em.remove(point);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
