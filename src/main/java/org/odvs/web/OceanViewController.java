package org.odvs.web;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.odvs.domain.LightPoint;
import org.odvs.service.PointService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping(value = "/oceanview")
public class OceanViewController {

	@Autowired
	PointService pointService;

	@RequestMapping(value = "index")
	public String index(Model map, HttpServletRequest request) {

		return "index";
	}

	@RequestMapping(value = "map", method = RequestMethod.GET)
	public String map(ModelMap map) {

		List<LightPoint> pointsL = pointService.getAllLightPoints();

		map.addAttribute("points", pointsL);
		return "sucess";
	}

	@RequestMapping(value = "point", method = RequestMethod.POST)
	public String getPoints(Integer id, ModelMap map) {

		String status = "success";

		map.addAttribute("point", pointService.getById(id));

		return "success";
	}

	@RequestMapping(value = "station", method = RequestMethod.POST)
	public String getStationPoints(Integer station, ModelMap map) {

		String status = "success";

		map.addAttribute("points", pointService.getAllPoint(station));

		return "success";
	}

	@RequestMapping(value = "points", method = RequestMethod.POST)
	public String getPoints(String lat, String lon, ModelMap map) {

		String status = "success";

		try {

			map.addAttribute("livraison", "null");

		} catch (Exception e) {
			status = "un probleme est survenu lors de la mise à jour ";
		}
		map.addAttribute("status", status);

		return "success";
	}

}
