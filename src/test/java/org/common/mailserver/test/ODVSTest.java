package org.common.mailserver.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.odvs.dao.PointDAO;
import org.odvs.service.PointService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:/META-INF/applicationContext.xml")
@TestExecutionListeners(DependencyInjectionTestExecutionListener.class)
public class ODVSTest {

	@Autowired
	PointService pointService;
	@Autowired
	PointDAO pointDAO;

	@Test
	public void testListPoints() {
		// System.out.println(pointService.getAllPoint());
		System.out.println(pointService.getAllLightPoints().size());
		// System.out.println(pointService.getAllPoint("-78.481", "-163.859"));
		// System.out.println(pointService.getAllPoint("-78.481", "-163.859"));
	}

}
