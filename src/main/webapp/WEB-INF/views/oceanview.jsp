<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<title>CO2 OCEAN View</title>
<style type="text/css">
div#map_container {
	width: 60%;
	height: 100%;
}
</style>

<script src='<c:url value="/resources/js/jquery-2.1.4.min.js"/>'></script>
<script type="text/javascript"
	src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>



</head>

<body onload="loadMap()">

	<div id="map_container"></div>
</body>

</html>